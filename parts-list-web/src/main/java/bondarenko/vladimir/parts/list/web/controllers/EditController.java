package bondarenko.vladimir.parts.list.web.controllers;

import bondarenko.vladimir.parts.list.domain.entityes.ComputerParts;
import bondarenko.vladimir.parts.list.service.ComputerPartsService;
import java.io.UnsupportedEncodingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class EditController {

    @Autowired
    private ComputerPartsService computerPartsService;

    @Transactional
    @RequestMapping(value = "edit", method = RequestMethod.GET)
    public String editUser(Model model,
            @RequestParam("computerPartsId") Integer computerPartsId) {
        model.addAttribute("computerParts", computerPartsService.byId(computerPartsId));
        return "edit";
    }

    @Transactional
    @RequestMapping(value = "edit", method = RequestMethod.POST)
    public String editUser(@RequestParam("computerPartsId") Integer computerPartsId,
            @RequestParam("name") String name,
            @RequestParam("necessity") Boolean necessity,
            @RequestParam("quantity") Integer quantity) throws UnsupportedEncodingException {
        ComputerParts computerParts = computerPartsService.byId(computerPartsId);
        String db_string = new String(name.getBytes("iso-8859-1"), "utf-8");
        computerParts.setName(db_string);
        computerParts.setNecessity(necessity);
        computerParts.setQuantity(quantity);
        computerPartsService.update(computerParts);
        return "redirect:home";
    }

    @Transactional
    @RequestMapping(value = "deleteComputerParts", method = RequestMethod.POST)
    public String deletePost(@RequestParam(value = "computerPartsId") int computerPartsId) {
        computerPartsService.delete(computerPartsService.byId(computerPartsId));
        return "redirect:home";
    }
}
