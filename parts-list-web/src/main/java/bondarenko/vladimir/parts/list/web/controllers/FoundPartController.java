package bondarenko.vladimir.parts.list.web.controllers;

import bondarenko.vladimir.parts.list.domain.entityes.ComputerParts;
import bondarenko.vladimir.parts.list.service.ComputerPartsService;
import java.io.UnsupportedEncodingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class FoundPartController {

    @Autowired
    private ComputerPartsService computerPartsService;

    @Transactional
    @RequestMapping(value = "foundPart", method = RequestMethod.POST)

    public String foundPart(@RequestParam(value = "foundPart") String foundPart) throws UnsupportedEncodingException {
        String enc_string = new String(foundPart.getBytes("iso-8859-1"), "utf-8");
        for (ComputerParts computerParts : computerPartsService.list()) {
            if (computerParts.getName().equalsIgnoreCase(enc_string.trim())) {
                return "redirect:edit?computerPartsId=" + computerParts.getId();
            }
        }
        return "redirect:home";
    }
}
