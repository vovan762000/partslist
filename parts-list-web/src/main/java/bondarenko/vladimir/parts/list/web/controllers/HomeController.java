package bondarenko.vladimir.parts.list.web.controllers;

import bondarenko.vladimir.parts.list.domain.entityes.ComputerParts;
import bondarenko.vladimir.parts.list.service.ComputerPartsService;
import java.util.ArrayList;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.support.PagedListHolder;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.servlet.ModelAndView;

@Controller
public class HomeController {

    @Autowired
    private ComputerPartsService computerPartsService;

    @Transactional
    @RequestMapping(value = "/home", method = RequestMethod.GET)
    public ModelAndView home(@RequestParam(required = false) Integer page) {
        int minCompQuantity = 0;
        for (ComputerParts el : computerPartsService.list()) {
            if (el.getNecessity() == true) {
                minCompQuantity = el.getQuantity();
                break;
            }
        }
        for (ComputerParts el : computerPartsService.list()) {
            if (el.getNecessity() == true && minCompQuantity > el.getQuantity()) {
                minCompQuantity = el.getQuantity();
            }
        }
        ModelAndView modelAndView = new ModelAndView("home");
        List<ComputerParts> computerPartses = computerPartsService.list();
        PagedListHolder<ComputerParts> pagedListHolder = new PagedListHolder<>(computerPartses);
        pagedListHolder.setPageSize(10);
        modelAndView.addObject("maxPages", pagedListHolder.getPageCount());

        if (page == null || page < 1 || page > pagedListHolder.getPageCount()) {
            page = 1;
        }

        modelAndView.addObject("page", page);

        if (page == null || page < 1 || page > pagedListHolder.getPageCount()) {
            pagedListHolder.setPage(0);
            modelAndView.addObject("computerPartses", pagedListHolder.getPageList());
        } else if (page <= pagedListHolder.getPageCount()) {
            pagedListHolder.setPage(page - 1);
            modelAndView.addObject("computerPartses", pagedListHolder.getPageList());
        }
        modelAndView.addObject("minCompQuantity", minCompQuantity);
        return modelAndView;
    }
    
    @Transactional
    @RequestMapping(value = "necessary", method = RequestMethod.GET)
    public ModelAndView necessary(@RequestParam(required = false) Integer page) {
        int minCompQuantity = 0;
        for (ComputerParts el : computerPartsService.list()) {
            if (el.getNecessity() == true) {
                minCompQuantity = el.getQuantity();
                break;
            }
        }
        for (ComputerParts el : computerPartsService.list()) {
            if (el.getNecessity() == true && minCompQuantity > el.getQuantity()) {
                minCompQuantity = el.getQuantity();
            }
        }
        List<ComputerParts> computerPartses = new ArrayList<>();
        for (ComputerParts computerParts : computerPartsService.list()) {
            if (computerParts.getNecessity()) {
                computerPartses.add(computerParts);
            }
        }
        ModelAndView modelAndView = new ModelAndView("home");
        
        PagedListHolder<ComputerParts> pagedListHolder = new PagedListHolder<>(computerPartses);
        pagedListHolder.setPageSize(10);
        modelAndView.addObject("maxPages", pagedListHolder.getPageCount());

        if (page == null || page < 1 || page > pagedListHolder.getPageCount()) {
            page = 1;
        }

        modelAndView.addObject("page", page);

        if (page == null || page < 1 || page > pagedListHolder.getPageCount()) {
            pagedListHolder.setPage(0);
            modelAndView.addObject("computerPartses", pagedListHolder.getPageList());
        } else if (page <= pagedListHolder.getPageCount()) {
            pagedListHolder.setPage(page - 1);
            modelAndView.addObject("computerPartses", pagedListHolder.getPageList());
        }
        modelAndView.addObject("minCompQuantity", minCompQuantity);
        return modelAndView;
    }
    
    @Transactional
    @RequestMapping(value = "optional", method = RequestMethod.GET)
    public ModelAndView optional(@RequestParam(required = false) Integer page) {
        int minCompQuantity = 0;
        for (ComputerParts el : computerPartsService.list()) {
            if (el.getNecessity() == true) {
                minCompQuantity = el.getQuantity();
                break;
            }
        }
        for (ComputerParts el : computerPartsService.list()) {
            if (el.getNecessity() == true && minCompQuantity > el.getQuantity()) {
                minCompQuantity = el.getQuantity();
            }
        }
        List<ComputerParts> computerPartses = new ArrayList<>();
        for (ComputerParts computerParts : computerPartsService.list()) {
            if (!computerParts.getNecessity()) {
                computerPartses.add(computerParts);
            }
        }
        ModelAndView modelAndView = new ModelAndView("home");
        
        PagedListHolder<ComputerParts> pagedListHolder = new PagedListHolder<>(computerPartses);
        pagedListHolder.setPageSize(10);
        modelAndView.addObject("maxPages", pagedListHolder.getPageCount());

        if (page == null || page < 1 || page > pagedListHolder.getPageCount()) {
            page = 1;
        }

        modelAndView.addObject("page", page);

        if (page == null || page < 1 || page > pagedListHolder.getPageCount()) {
            pagedListHolder.setPage(0);
            modelAndView.addObject("computerPartses", pagedListHolder.getPageList());
        } else if (page <= pagedListHolder.getPageCount()) {
            pagedListHolder.setPage(page - 1);
            modelAndView.addObject("computerPartses", pagedListHolder.getPageList());
        }
        modelAndView.addObject("minCompQuantity", minCompQuantity);
        return modelAndView;
    }
    
    @RequestMapping(value = "/getTags", method = RequestMethod.GET)
    public @ResponseBody
    List<Tag> getTags(@RequestParam String tagName) {
        return simulateSearchResult(tagName);
    }
    
    private List<Tag> simulateSearchResult(String tagName) {
        
        List<Tag> result = new ArrayList<Tag>();
        List<Tag> data = new ArrayList<Tag>();
        
        for (ComputerParts computerParts : computerPartsService.list()) {
                data.add(new Tag(computerParts));           
        }
        // iterate a list and filter by tagName
        for (Tag tag : data) {
            if (tag.getTagName().toLowerCase().contains(tagName.toLowerCase())) {
                result.add(tag);
            }
        }
        
        return result;
    }
    
    public class Tag {
        
        public int id;
        public String tagName;
        
        public int getId() {
            return id;
        }
        
        public void setId(int id) {
            this.id = id;
        }
        
        public String getTagName() {
            return tagName;
        }
        
        public void setTagName(String tagName) {
            this.tagName = tagName;
        }
        
        public Tag(ComputerParts computerParts) {
            this.id = computerParts.getId();
            this.tagName = computerParts.getName();
        }
        
    }
}
