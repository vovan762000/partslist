package bondarenko.vladimir.parts.list.web.controllers;

import bondarenko.vladimir.parts.list.domain.entityes.ComputerParts;
import bondarenko.vladimir.parts.list.service.ComputerPartsService;
import java.io.UnsupportedEncodingException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class AddComputerPartController {

    @Autowired
    private ComputerPartsService computerPartsService;

    @Transactional
    @RequestMapping(value = "addComputerPart", method = RequestMethod.GET)
    public String addPart() {
        return "addComputerPart";
    }

    @Transactional
    @RequestMapping(value = "addComputerPart", method = RequestMethod.POST)
    public String addPart(
            @RequestParam("name") String name,
            @RequestParam("necessity") Boolean necessity,
            @RequestParam("quantity") Integer quantity) throws UnsupportedEncodingException {
        ComputerParts newComputerParts = new ComputerParts();
        String db_string = new String(name.getBytes("iso-8859-1"), "utf-8");
        if (name != null && necessity != null && quantity != null) {
            newComputerParts.setName(db_string);
            newComputerParts.setNecessity(necessity);
            newComputerParts.setQuantity(quantity);
            computerPartsService.save(newComputerParts);
            return "redirect:home";
        }
        return "addComputerPart";
    }
}
