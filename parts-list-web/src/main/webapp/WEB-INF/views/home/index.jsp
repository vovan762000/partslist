<%@ page contentType="text/html; charset=UTF-8" language="java" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>

<!DOCTYPE html>
<html>
    <head>
        <meta charset="utf-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta name="description" content="">
        <meta name="author" content="">

        <title></title>

        <link href="<c:url value="static/css/index.css" />" rel="stylesheet">
        <link href="<c:url value="static/css/indexcustom.css" />" rel="stylesheet">

    </head>
    <body>

        <div class="container">

            <div class="jumbotron" style="margin-top: 20px;">
                <h1>Parts List</h1>
                <p><a class="btn btn-lg btn-success" href="<c:url value="/home" />" role="button">show the list of accessories</a></p>
            </div>

            <div class="footer">
                <p>© Vladimir Bondarenko 2018</p>
            </div>

        </div>
    </body>
</html>
