<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%> 

<head>
    <link href="static/css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="static/css/sb-admin.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="static/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
</head>
<body>
    <div id="page-wrapper">
            <div class="container-fluid">             
                <div class="row">
                    <div class="col-lg-12">
                        <div class="table-responsive">
                            <form:form  method="POST" action="addComputerPart">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th colspan="2">Добавить позицию</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <tr>
                                            <td>Наименование</td>
                                            <td>
                                                <input class="form-control" type="text" name="name"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Необходимость</td>
                                            <td>
                                                <input class="form-control" type="text" name="necessity"/>
                                            </td>
                                        </tr>
                                        <tr>
                                            <td>Количество</td>
                                            <td>
                                                <input class="form-control" type="text" name="quantity"/>
                                            </td>
                                        </tr>
                                    </tbody>
                                </table>
                                <input class="btn btn-success" type="submit" value="сохранить и на главную"/>
                            </form:form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
</body>
</html>
