<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="fmt"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>

<html>
    <head>
        <link href="static/css/bootstrap.min.css" rel="stylesheet">

        <!-- Custom CSS -->
        <link href="static/css/sb-admin.css" rel="stylesheet">

        <!-- Custom Fonts -->
        <script src="static/js/jquery.js"></script>

        <!-- Bootstrap Core JavaScript -->
        <script src="static/js/bootstrap.min.js"></script>
        <link href="static/font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
        <script src="<c:url value="static/js/jquery.1.10.2.min.js" />"></script>
        <script src="<c:url value="static/js/jquery.autocomplete.min.js" />"></script>
        <link href="<c:url value="static/css/main.css" />" rel="stylesheet">
    </head>
    <body>
        <div id="page-wrapper">
            <div class="container-fluid">             
                <div class="row">
                    <div class="col-lg-12">
                        <h2>Компьтерные комплектующие</h2>
                        <div class="container-fluid"> 
                            <form:form action="foundPart" method="POST">
                                <div class="form-group">
                                    <input type="text" name="foundPart" id="w-input-search" size="40 class="form-control" placeholder="Введите название комплектующеего" >
                                           <span>
                                        <button id="button-id" type="submit">Поиск</button>
                                    </span>
                                </div>
                            </form:form>
                            <script>
                                $(document).ready(function () {

                                    $('#w-input-search').autocomplete({
                                        serviceUrl: '${pageContext.request.contextPath}/getTags',
                                        paramName: "tagName",
                                        delimiter: ",",
                                        transformResult: function (response) {

                                            return {
                                                //must convert json to javascript object before process
                                                suggestions: $.map($.parseJSON(response), function (item) {

                                                    return {value: item.tagName, data: item.id};
                                                })

                                            };

                                        }

                                    });

                                });
                            </script>
                            <br/> 
                            <ul class="nav">
                                <li class="nav-item">
                                    <a class="nav-link active" href="home">Все</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="necessary">Необходимы для сборки</a>
                                </li>
                                <li class="nav-item">
                                    <a class="nav-link" href="optional">Опциональные детали</a>
                                </li>
                            </ul>
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                        <tr>
                                            <th>Наименование</th>
                                            <th>Необходимость</th>
                                            <th>Количество</th>

                                        </tr>
                                    </thead>
                                    <tbody>
                                        <c:forEach var="computerPartses" items="${computerPartses}" varStatus="number">
                                            <tr>                                         
                                                <td>${computerPartses.name}</td>
                                                <c:if test="${computerPartses.necessity == true}">
                                                    <td>да</td>
                                                </c:if>
                                                <c:if test="${computerPartses.necessity == false}">
                                                    <td>нет</td>
                                                </c:if>
                                                <td>${computerPartses.quantity}</td> 
                                                <td><a href="edit?computerPartsId=${computerPartses.id}" class="btn btn-md btn-success">Редактировать</a></td>
                                            </tr>
                                        </c:forEach>
                                    </tbody>
                                </table>                 
                            </div>
                            <div id="pagination">
                                <c:url value="/home" var="prev">
                                    <c:param name="page" value="${page-1}"/>
                                </c:url>
                                <c:if test="${page > 1}">
                                    <a href="<c:out value="${prev}" />" class="pn prev">Предыдущая</a>
                                </c:if>
                                <c:forEach begin="1" end="${maxPages}" step="1" varStatus="i">
                                    <c:choose>
                                        <c:when test="${page == i.index}">
                                            <span>${i.index}</span>
                                        </c:when>
                                        <c:otherwise>
                                            <c:url value="/home" var="url">
                                                <c:param name="page" value="${i.index}"/>
                                            </c:url>
                                            <a href='<c:out value="${url}" />'>${i.index}</a>
                                        </c:otherwise>
                                    </c:choose>
                                </c:forEach>
                                <c:url value="/home" var="next">
                                    <c:param name="page" value="${page + 1}"/>
                                </c:url>
                                <c:if test="${page + 1 <= maxPages}">
                                    <a href='<c:out value="${next}" />' class="pn next">Следующая</a>
                                </c:if>
                                <br><br>
                                <a href="addComputerPart" class="btn btn-md btn-success">Добавить позицию</a>
                                <br><br>
                                <table class="table table-bordered table-hover">                      
                                    <tbody>
                                        <tr>   
                                            <td>Можно собрать</td>
                                            <td>${minCompQuantity}</td> 
                                            <td>компьютеров</td>
                                        </tr>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                    </body>
                    </html>
