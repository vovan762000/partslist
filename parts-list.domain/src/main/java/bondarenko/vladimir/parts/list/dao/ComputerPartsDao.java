
package bondarenko.vladimir.parts.list.dao;

import bondarenko.vladimir.parts.list.domain.entityes.ComputerParts;

public interface ComputerPartsDao extends BaseDao<ComputerParts,Integer>{
    
}
