
package bondarenko.vladimir.parts.list.daoimpl;

import bondarenko.vladimir.parts.list.dao.AbstractDao;
import bondarenko.vladimir.parts.list.dao.ComputerPartsDao;
import bondarenko.vladimir.parts.list.domain.entityes.ComputerParts;
import org.springframework.stereotype.Component;

@Component
public class ComputerPartsDaoImpl extends AbstractDao<ComputerParts,Integer> implements ComputerPartsDao{
    
}
