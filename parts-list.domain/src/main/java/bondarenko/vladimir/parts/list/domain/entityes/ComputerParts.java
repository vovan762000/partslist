
package bondarenko.vladimir.parts.list.domain.entityes;

import java.io.Serializable;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.validation.constraints.NotNull;
import javax.validation.constraints.Size;


@Entity
@Table(name = "computer_parts")
@NamedQueries({
@NamedQuery(name = "ComputerParts.findAll", query = "SELECT c FROM ComputerParts c")})
public class ComputerParts implements Serializable {

    private static final long serialVersionUID = 1L;
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private Integer id;
    
    @Basic(optional = false)
    @NotNull
    @Size(min = 1, max = 45)
    @Column(name = "name")
    private String name;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "necessity")
    private boolean necessity;
    
    @Basic(optional = false)
    @NotNull
    @Column(name = "quantity")
    private int quantity;

    public ComputerParts() {
    }

    public ComputerParts(Integer id) {
        this.id = id;
    }

    public ComputerParts(String name, boolean necessity, int quantity) {
        this.name = name;
        this.necessity = necessity;
        this.quantity = quantity;
    }

    public ComputerParts(Integer id, String name, boolean necessity, int quantity) {
        this.id = id;
        this.name = name;
        this.necessity = necessity;
        this.quantity = quantity;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public boolean getNecessity() {
        return necessity;
    }

    public void setNecessity(boolean necessity) {
        this.necessity = necessity;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public int hashCode() {
        int hash = 0;
        hash += (id != null ? id.hashCode() : 0);
        return hash;
    }

    @Override
    public boolean equals(Object object) {
        if (!(object instanceof ComputerParts)) {
            return false;
        }
        ComputerParts other = (ComputerParts) object;
        if ((this.id == null && other.id != null) || (this.id != null && !this.id.equals(other.id))) {
            return false;
        }
        return true;
    }

    @Override
    public String toString() {
        return "bondarenko.vladimir.parts.list.domain.entityes.ComputerParts[ id=" + id + " ]";
    }
    
}
