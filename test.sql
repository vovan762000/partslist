CREATE TABLE `test`.`computer_parts` (
  `id` INT NOT NULL AUTO_INCREMENT,
  `name` VARCHAR(45) NOT NULL,
  `necessity` TINYINT(1) NOT NULL,
  `quantity` INT NOT NULL,
  PRIMARY KEY (`id`));
INSERT INTO `test`.`computer_parts` (`name`, `necessity`, `quantity`) VALUES ('материнская плата', '1', '5');
INSERT INTO `test`.`computer_parts` (`name`, `necessity`, `quantity`) VALUES ('звуковая карта', '0', '3');
INSERT INTO `test`.`computer_parts` (`name`, `necessity`, `quantity`) VALUES ('процессор', '1', '9');
INSERT INTO `test`.`computer_parts` (`name`, `necessity`, `quantity`) VALUES ('подсветка корпуса', '0', '0');
INSERT INTO `test`.`computer_parts` (`name`, `necessity`, `quantity`) VALUES ('HDD диск', '0', '1');
INSERT INTO `test`.`computer_parts` (`name`, `necessity`, `quantity`) VALUES ('корпус', '1', '10');
INSERT INTO `test`.`computer_parts` (`name`, `necessity`, `quantity`) VALUES ('память', '1', '10');
INSERT INTO `test`.`computer_parts` (`name`, `necessity`, `quantity`) VALUES ('SSD диск', '1', '15');
INSERT INTO `test`.`computer_parts` (`name`, `necessity`, `quantity`) VALUES ('видеокарта', '0', '3');
INSERT INTO `test`.`computer_parts` (`name`, `necessity`, `quantity`) VALUES ('мышка', '1', '13');
INSERT INTO `test`.`computer_parts` (`name`, `necessity`, `quantity`) VALUES ('клавиатура', '1', '10');
INSERT INTO `test`.`computer_parts` (`name`, `necessity`, `quantity`) VALUES ('корпусной кулер', '0', '7');
INSERT INTO `test`.`computer_parts` (`name`, `necessity`, `quantity`) VALUES ('принтер', '0', '2');
INSERT INTO `test`.`computer_parts` (`name`, `necessity`, `quantity`) VALUES ('сканер', '0', '1');
INSERT INTO `test`.`computer_parts` (`name`, `necessity`, `quantity`) VALUES ('внешний SSD', '0', '9');
INSERT INTO `test`.`computer_parts` (`name`, `necessity`, `quantity`) VALUES ('дисковод', '0', '2');
INSERT INTO `test`.`computer_parts` (`name`, `necessity`, `quantity`) VALUES ('монитор', '1', '9');
INSERT INTO `test`.`computer_parts` (`name`, `necessity`, `quantity`) VALUES ('оптический привод', '0', '1');
INSERT INTO `test`.`computer_parts` (`name`, `necessity`, `quantity`) VALUES ('ИБП', '0', '4');
INSERT INTO `test`.`computer_parts` (`name`, `necessity`, `quantity`) VALUES ('сетевой фильтр', '0', '5');
INSERT INTO `test`.`computer_parts` (`name`, `necessity`, `quantity`) VALUES ('роутер', '0', '6');

