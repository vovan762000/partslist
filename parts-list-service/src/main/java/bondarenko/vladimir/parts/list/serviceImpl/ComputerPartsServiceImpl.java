package bondarenko.vladimir.parts.list.serviceImpl;

import bondarenko.vladimir.parts.list.dao.ComputerPartsDao;
import bondarenko.vladimir.parts.list.domain.entityes.ComputerParts;
import bondarenko.vladimir.parts.list.service.ComputerPartsService;
import java.io.Serializable;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.transaction.annotation.Transactional;

@Transactional
@Component
public class ComputerPartsServiceImpl implements ComputerPartsService {

    @Autowired
    private ComputerPartsDao computerPartsDao;

    @Override
    public ComputerParts byId(Integer id) {
        return computerPartsDao.byId(id);
    }

    @Override
    public Serializable save(ComputerParts object) {
        return computerPartsDao.save(object);
    }

    @Override
    public void update(ComputerParts object) {
        computerPartsDao.update(object);
    }

    @Override
    public void delete(ComputerParts object) {
        computerPartsDao.remove(object);
    }

    @Override
    public List<ComputerParts> list() {
        return computerPartsDao.list();
    }
}
