
package bondarenko.vladimir.parts.list.service;

import bondarenko.vladimir.parts.list.domain.entityes.ComputerParts;

public interface ComputerPartsService extends BaseService<ComputerParts, Integer>{
    
}
